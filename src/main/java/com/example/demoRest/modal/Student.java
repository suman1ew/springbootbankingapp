package com.example.demoRest.modal;

import lombok.Data;

@Data
public class Student {

    String name;
    int age;
    String registrationNumber;
}
