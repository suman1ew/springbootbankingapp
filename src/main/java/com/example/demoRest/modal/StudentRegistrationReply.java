package com.example.demoRest.modal;

import lombok.Data;

@Data
public class StudentRegistrationReply {

    String name;
    int age;
    String registrationNumber;
    String registrationStatus;

}
