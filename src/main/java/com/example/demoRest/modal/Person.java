package com.example.demoRest.modal;

import lombok.Data;



@Data
public class Person {

    private String first_name;
    private String last_name;
    private Address address;
    private String employment;
    private String martial;
    private int age;
    private String created_date;


}
