package com.example.demoRest.modal;

import lombok.Data;

@Data
public class Address {

    private String addressLine01;
    private String addressLine02;
    private String City;
    private String Country;
    private String pincode;
}
