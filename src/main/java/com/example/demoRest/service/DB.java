package com.example.demoRest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.sql.Connection;
import java.sql.DriverManager;



public class DB {


    @Value("${spring.datasource.url}" )
    String url;
    @Value("${resources.spring.datasource.username}" )
    String username;
    @Value("${spring.datasource.password}" )
    String Password;
    public  Connection getConnection(){


        Connection con=null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con= DriverManager.getConnection(url,username,Password);
        }catch(Exception e){System.out.println(e);}
        return con;
    }

}