package com.example.demoRest.resource;

import com.example.demoRest.modal.StudentRegistration;
import org.springframework.web.bind.annotation.*;

@RestController
public class StudentDeleteController {

    @RequestMapping(method = RequestMethod.DELETE, value="/delete/student/{regdNum}")

    @ResponseBody
    public String deleteStudentRecord(@PathVariable("regdNum") String regdNum) {
        System.out.println("In deleteStudentRecord");
        return StudentRegistration.getInstance().deleteStudent(regdNum);
    }

}
