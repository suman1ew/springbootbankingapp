package com.example.demoRest.resource;

import com.example.demoRest.modal.Person;
import com.example.demoRest.modal.PersonReply;
import com.example.demoRest.service.DB;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;
@RestController
public class PersonController {
    @RequestMapping(method = RequestMethod.POST, value = "/create/person")

    @ResponseBody
    public PersonReply createPerson(@RequestBody Person person) {
   /* DB db=new DB();
        if(db.getConnection().equals(true));
        else
            System.exit(0);*/
        PersonReply persondata=new PersonReply();
        persondata.setPerson_id(UUID.randomUUID().toString());
        persondata.setFirst_name(person.getFirst_name());
        persondata.setLast_name(person.getLast_name());
        persondata.setAddress(person.getAddress());
        persondata.setEmployment(person.getEmployment());
        persondata.setMartial(person.getMartial());
        persondata.setAge(person.getAge());
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        System.out.println(formatter.format(calendar.getTime()));
        persondata.setCreated_date(formatter.format(calendar.getTime()));

        return persondata;



    }

}
