package com.example.demoRest.resource;

import com.example.demoRest.modal.Student;
import com.example.demoRest.modal.StudentRegistration;
import com.example.demoRest.modal.StudentRegistrationReply;
import org.springframework.web.bind.annotation.*;


@RestController
public class StudentRegistrationController {

    @RequestMapping(method = RequestMethod.POST, value = "/register/student")

    @ResponseBody
    public StudentRegistrationReply registerStudent(@RequestBody Student student) {
        System.out.println("In registerStudent");
        StudentRegistrationReply stdregreply = new StudentRegistrationReply();
        StudentRegistration.getInstance().add(student);
        //We are setting the below value just to reply a message back to the caller
        stdregreply.setName(student.getName());
        stdregreply.setAge(student.getAge());
        stdregreply.setRegistrationNumber(student.getRegistrationNumber());
        stdregreply.setRegistrationStatus("Successful");

        return stdregreply;
    }
}
