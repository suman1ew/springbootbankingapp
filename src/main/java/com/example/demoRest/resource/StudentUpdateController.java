package com.example.demoRest.resource;

import com.example.demoRest.modal.Student;
import com.example.demoRest.modal.StudentRegistration;
import org.springframework.web.bind.annotation.*;


@RestController
public class StudentUpdateController {

    @RequestMapping(method = RequestMethod.PUT, value="/update/student")


    @ResponseBody
    public String updateStudentRecord(@RequestBody Student stdn) {
        System.out.println("In updateStudentRecord");
        return StudentRegistration.getInstance().upDateStudent(stdn);
    }

}
